# Recieve a List of Morphologies
# Flag To Create SWC file
# Folder Directory

# Return dictionary
#   {json_filename:json_contents_1,xmi_filename,xmi_contents,swc_filename_1:swc_contents_1,...}

import json
import numpy as np
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
###############################
# create a geppetto project class
# give it a set of morphologies
# it generates a name for each swc
# creates a dictionary 
# can create the files if required.
###############################

def rgb_to_hex(rgb):
    rgb = tuple([int(c*255) for c in rgb])
    return '0x%02x%02x%02x' % rgb

class geppetto_project(object):
    #
    # Needs a project id to be passed in
    # Needs a URL dir

    
    def __init__(self, morphologies, name="demo", url_dir='SERVER_ROOT/app_data/',create_swc=True):
        self.project = {}
        self.url_dir = url_dir
        self.name = name
        self.file_dir = url_dir + name + "/"
        self.json_file = self.file_dir + 'Specification.json'
        self.SERVER_ROOT = '/opt/virgo-tomcat-server-3.6.3.RELEASE/app_data/' +name # HERE: Needs to be set for the actual file creation
                               # And passed into the create method

        self.morphologies = morphologies        # Update to a pandas dataframe?

        self.morphologies['file_id'] = pd.Series(["Neuron_" +str( number) for number in np.arange(len(self.morphologies['x']))],  index=self.morphologies.index)

        if create_swc:
            self.add_swc(self.morphologies)    
        self.add_xmi(self.morphologies,self.file_dir)
        self.add_script(self.morphologies)
        self.add_json(self.file_dir)
        self.create_files()
    
    def add_script(self,morphologies):
        self.project['Script.js'] = geppetto_script(morphologies).output

    def add_xmi(self,morphologies,url_dir):
        self.project['GeppettoModel.xmi'] = geppetto_xmi(morphologies,self.file_dir).output
    
    def add_json(self,url_dir):
        self.project['Specification.json'] = geppetto_json(self.name,self.file_dir,
                                                           self.get_color_map()).output
    
    def add_swc(self,morphologies):
        for mi in morphologies.index.values:
            m = morphologies.loc[mi]
            self.project[str(m['file_id'])+'.swc'] = geppetto_swc(m).output
    
    def create_files(self):
        local_dir = self.SERVER_ROOT
        if not os.path.exists(local_dir):
            os.makedirs(local_dir)
        for file_name in self.project.keys():
            f = local_dir+"/" + file_name
            with open(f, "w") as out_file:
                out_file.write(self.project[file_name])
                
    def get_color_map(self):
        morphologies=self.morphologies
        total = len(morphologies.index.values)
        mp = {}
        for (mi,i) in zip(morphologies.index.values,(np.arange(0,total+1)/float(total+1))[1:]):
            m = morphologies.loc[mi]
            filename = m['file_id']
            #cmap = plt.cm.get_cmap('Accent')
            cmap = plt.cm.get_cmap('gist_rainbow')
            #col = (i * 50) % 256
            mp[filename]={'color':rgb_to_hex(cmap(i)[:3]),
                          'opacity':0.4}
        mp['linesThreshold'] = 100000
        return mp

class geppetto_swc(object):
    def __init__(self,m):
        #assert type(m) is MorphologyData
        swc = []
        
        if 'p' in m:
            m['parent'] = m['p']

        for j in (np.arange(len(m['x'].keys()))):
            i = str(j)
            swc.append("%(l)s 0 %(x)s %(y)s %(z)s %(r)s %(p)s\n" % \
                {'l': str(j+1), 'x': m['x'][i], 'y': m['y'][i], \
                'z': m['z'][i], 'r': m['r'][i], \
                'p': m['parent'][i]})
        
        self.output = "".join(swc) 

    def output(self):
        return self.output()

class geppetto_script(object):

    def __init__(self,morphologies):
        self.script_template = """
        c = GEPPETTO.getVARS().camera;
        c.up.x = 0;
        c.up.y = 0;
        c.up.z = -1;
        R = 15000;
        R = Math.sqrt(Math.pow(c.position.x - GEPPETTO.getVARS().sceneCenter.x,2) + Math.pow(c.position.y - GEPPETTO.getVARS().sceneCenter.y,2) + Math.pow(c.position.z - GEPPETTO.getVARS().sceneCenter.z,2));
        c.position.x = GEPPETTO.getVARS().sceneCenter.x + R;
        c.position.y = GEPPETTO.getVARS().sceneCenter.y ;
        c.position.z = GEPPETTO.getVARS().sceneCenter.z ;
        function setTheta(c,theta, R) { c.up.x = 0;c.up.y = 0;c.up.z = -1;c.position.x = GEPPETTO.getVARS().sceneCenter.x + R*Math.cos(Math.PI * theta/180);  c.position.y = GEPPETTO.getVARS().sceneCenter.y + R*Math.sin(Math.PI * theta/180) ; console.log(c.position) ;}

document.getElementById("github").style.visibility = "hidden";
document.getElementById("consoleButton").style.visibility = "hidden";
document.getElementById("geppettologo").style.visibility = "hidden";
document.getElementById("experimentsButton").style.visibility = "hidden";
document.getElementsByClassName("homeButton")[0].style.visibility = "hidden";
        """
        i = 0
        total = len(morphologies.index.values)
        vals = (np.arange(0,total+1)/float(total+1))[1:]
        for mi in morphologies.index.values:
            m = morphologies.loc[mi]
            filename = m['file_id']
            cmap = plt.cm.get_cmap('gist_rainbow')
            #col = (i * 50) % 256
            col = vals[i]
            self.script_template += '''%s.setColor(%s)
                    %s.setOpacity(0.4)

                    ''' % (filename,rgb_to_hex(cmap(col)[:3]),filename)
            i = i+1
        
        self.output = self.script_template
        

    def output(self):
        return self.output
    


class geppetto_json(object):

    def __init__(self,e_id,url_dir='SERVER_ROOT/app_data/',mp={}):
        # Temporary hack to pass colors in description
        self.file_template = '''{{
    "id": 1,
    "name": "Automatically Generated Template",
    "activeExperimentId": 1,
    "experiments": [
        {{
            "id": 1,
            "name": "NeuroArch Morphology Only",
            "status": "COMPLETED",  
            "description" : "{0}",
            "lastModified":"1436102517799",
            "script":"{1}Script.js",
            "aspectConfigurations": []
        }}
    ],
    "geppettoModel": {{
        "id": 1,
        "url": "{1}GeppettoModel.xmi",
        "type": "GEPPETTO_PROJECT"
    }}
}}'''
        self.output = self.file_template.format(str(mp).replace("'",'\\"'),url_dir)

    def output(self):
        return self.output

class geppetto_xmi(object):

    
    def __init__(self,ms=None,url_dir='SERVER_ROOT/app_data/'):
        # Load Template
        self.var_template =   '<variables id="%(id)s" name="%(name)s" types="//@libraries.0/@types.%(num)s"/>'

        self.type_template = '<types xsi:type="gep_1:ImportType" id="%(id)s" url="%(url)s" modelInterpreterId="swcModelInterpreter"/>'    

        self.file_template = '''<?xml version="1.0" encoding="ASCII"?>
<gep:GeppettoModel
    xmi:version="2.0"
    xmlns:xmi="http://www.omg.org/XMI"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:gep="https://raw.githubusercontent.com/openworm/org.geppetto.model/master/src/main/resources/geppettoModel.ecore"
    xmlns:gep_1="https://raw.githubusercontent.com/openworm/org.geppetto.model/master/src/main/resources/geppettoModel.ecore#//types">
    %(variables)s
  <libraries id="swc" name="NeuroArch">
      %(types)s
  </libraries>
</gep:GeppettoModel>'''

        self.variables = []
        self.types = []
        self.url_dir = url_dir   
        num = 0
        for mi in ms.index.values:
            m = ms.loc[mi]
            self.add_morphology(m,str(num))
            num = num +1

        self.output = self.output()
        

    def add_morphology(self,m,num):
        """ Load a Morphology Object into the xmi file """
        # extract id, name, url
        m_id = m['file_id']
        m_name = m['name']
        m_url = self.url_dir + str(m['file_id']) +'.swc'
        
        self.add_variable(m_id,str(m_name),str(num))
        self.add_type(m_id,m_url)
    
    def add_variable(self,m_id,m_name,m_num):
        self.variables.append(self.var_template % {'id':m_id,'name':m_name,'num':m_num})

    def add_type(self,m_id,m_url):
        self.types.append(self.type_template % {'id':m_id,'url':m_url})                

    def output(self):
        return self.file_template % {'variables': " ".join(self.variables),'types': " ".join(self.types)}


