import argparse
import sys
import zerorpc
import json

# Required to create temporary morphology files
import string
import random

# File generation code
from geppetto_server.gep_gen import generator
import pandas as pd

import urllib2
"""
test the server is running from the command line using:
	zerorpc -j tcp://localhost:4244 test
"""

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def create_swc_file(morphology):
    swc = []
    
    for i in morphology['x'].keys():
        swc.append("%(l)s %(x)s %(y)s %(z)s %(r)s %(p)s\n" % \
            {'l': i, 'x': morphology['x'][i], 'y': morphology['y'][i], \
            'z': morphology['z'][i], 'r': morphology['r'][i], 'p': morphology['p'][i]})
    
    return swc

def main(argv=None):

    """ run the main function and set up a listening server """
    class geppetto_server(object):
        """ Methods which are bound to zerorpc server """

        def __init__(self,args):
            # Get public ip for this server 
            ret = urllib2.urlopen('https://enabledns.com/ip')
            ip = ret.read()

            self.ping_msg = {   'name':args.name,
                                'ip': ip,
                                'port': args.port,
                                'callback':'ping',
                                'type':'vis_server'
                            }
            self.pro_address = args.pro_address
            self.pro_port = args.pro_port
            self.status = self.register(self.pro_address,self.pro_port)

        def ping(self):
            """ retutn the dictionary object that can identify the server """
            return json.dumps(self.ping_msg)

        def register(self,processor_ip,processor_port):
            """ register the module with a processor"""
            connect_to = "tcp://" + processor_ip + ":" + processor_port
            print "attempting registration with server " + connect_to
            p = zerorpc.Client()
            try:
                print "connecting..."
                p.connect(connect_to)
                print "connected"
            except:
                print "ERROR: connection failed"
                return False

            try:
                print "register to server..."
                result = p.add_server(json.dumps(self.ping_msg))
                print "registered"
            except:
                del p
                print "ERROR: add server method failed"
                return False
                del p
            return result

        def test(self):
            """ echo a test sentence from the server """
            print "Testing Accessed"
            return "Geppetto Server : Testing, Testing, 1 2 3"
        
        def visualise(self,ms, name="demo", url_dir='SERVER_ROOT/app_data/',create_swc=True):
            """ generate a set of files for geppetto to display  """
            ms = pd.read_json(ms)
            
            name  = name + id_generator(4)            

            g = generator.geppetto_project(ms,name,url_dir,create_swc)
            g.create_files()
            return str(name)



    parser = argparse.ArgumentParser()

    parser.add_argument('-a', '--address', default='0.0.0.0',type=str,help='the ip address to accept connections from [default 0.0.0.0 accepts all incoming connections]')
    parser.add_argument('-p', '--port', default='4244',type=str,help='the port to listen for connections on [default 4244]')
    parser.add_argument('-n', '--name', default='vis_server',type=str,help='the name of the server to register with a central processor')
    parser.add_argument('-b', '--pro_address', default='127.0.0.1',type=str,help='the ip address of the processor to connect to [default 127.0.0.1]')
    parser.add_argument('-q', '--pro_port', default='4245',type=str,help='the port to connect to a processor with [default 4245]')


    args = parser.parse_args()
    
    print "geppetto_server listening on tcp://%s:%s" %(args.address,args.port)
    
    s = zerorpc.Server(geppetto_server(args))
    s.bind("tcp://%s:%s" %(args.address,args.port))
    s.run()

if __name__ == "__main__":
    sys.exit(main())
