## Synopsis

A project to enable dynamic geppetto file generation, in order to support dynamic visualisation using the geppetto load_by_url capability.

## Code Example


## Motivation

We require the abiity to dynamically display arbitary morphology data in the Geppetto system, without the requirement for a pre-built project. This repository will all us to dynamically create the required files, such that the output can be inately visualised using the load_by_url

## Installation

WIP

## API Reference

WIP

## Tests

WIP

## Contributors

Adam Tomkins

## License

This software is licensed under the `BSD License
