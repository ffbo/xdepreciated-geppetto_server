# README #

This Docker File will install and launch the Geppetto Visualisation platform

### What is this repository for? ###

* Build Dockerfile
* Run Dockerfile
* Run Geppetto Services

### Build ###

 build -t geppetto_server:dockerfile .

### Run Container ###

This will run the docker as a service, start the geppetto engine, and bind the port to 8080 on the host machine

docker run -d -i -t -p 8080:8080 geppetto_server:dockerfile sh geppetto/bin/startup.sh